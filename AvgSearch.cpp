#include "AvgSearch.h"
#include <iostream>

bool AvgSearch::cond(const Student& c) const {
	return c.mark >= 4;
}
