#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "PairEnumerator.h"
#include "AvgSearch.h"

TEST_CASE("General") {
	SECTION("true") {
		PairEnumerator pe("input.txt");
		
		AvgSearch s;
		s.addEnumerator(&pe);
		s.run();

		CHECK(s.found());
	}
	
	SECTION("false") {
		PairEnumerator pe("input2.txt");
		
		AvgSearch s;
		s.addEnumerator(&pe);
		s.run();

		CHECK_FALSE(s.found());
	}
}

