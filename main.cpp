#include <iostream>
#include "PairEnumerator.h"
#include "AvgSearch.h"

int main() {
	PairEnumerator pe("input.txt");
	
	AvgSearch s;
	s.addEnumerator(&pe);
	s.run();

	std::cout << "result = " ;
	if (s.found()) std::cout << "true";
	else std::cout << "false";
	std::cout << std::endl;

	return 0;
}
