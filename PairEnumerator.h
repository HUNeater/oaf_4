#pragma once

#include "seqinfileenumerator.hpp"
#include "enumerator.hpp"

struct Student {
	std::string code;
	int mark;

	friend std::istream& operator>>(std::istream &in, Student &s) {
		in >> s.code >> s.mark;
		return in;
	}
};

class PairEnumerator : public Enumerator<Student> {
protected:
	SeqInFileEnumerator<Student> *enor;
	Student c;
	bool bEnd;
	const char* path;

public:
	PairEnumerator(const char* path);
	~PairEnumerator();
	void first();
	void next();
	bool end() const { return bEnd; }
	Student current() const { return c; }
};
