#include "PairEnumerator.h"
#include <iostream>
#include "AverageCalc.h"

PairEnumerator::PairEnumerator(const char* path) {
	this->path = path;
	try {
		enor = new SeqInFileEnumerator<Student>(path);
	} catch (SeqInFileEnumerator<Student>::Exceptions e) {
		if (e == SeqInFileEnumerator<Student>::OPEN_ERROR) {
			std::cout << "nem letezik a fajl" << std::endl;
		} else {
			std::cout << "nem tudom mi a baj:(" << std::endl;
		}

		std::exit(-1);
	}
}

void PairEnumerator::first() {
	enor->first();
	next();
}

void PairEnumerator::next() {
	if (bEnd = enor->end()) return;
	
	c.code = enor->current().code;
	AverageCalc avg(c);
	avg.addEnumerator(enor);
	avg.run();

	c.mark = avg.result();
	std::cout << "avg for " << c.code << ": " << c.mark << std::endl;
}

PairEnumerator::~PairEnumerator() {
	delete enor;
}
