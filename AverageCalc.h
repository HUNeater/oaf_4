#pragma once

#include "summation.hpp"
#include "PairEnumerator.h"

class AverageCalc : public Summation<Student, double> {
protected:
	double sum;
	int count;
	std::string code;
	
	void first() {};
	bool whileCond(const Student &c) const;
	void init() override;
	void add(const Student& t) override;

public:
	AverageCalc(const Student& c) : Summation<Student, double>(), code(c.code) {}
};
