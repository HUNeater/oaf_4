#include "AverageCalc.h"
#include <iostream>

void AverageCalc::init() {
	*Summation<Student, double>::_result = 0;
	sum = 0.0;
	count = 0;
}

void AverageCalc::add(const Student &t) {
	++count;
	sum += t.mark;
	std::cout << "working on student with code: " << t.code << std::endl;
	*Summation<Student, double>::_result = sum / count;
}

bool AverageCalc::whileCond(const Student &c) const {
	return code == c.code;
}
